class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def after_sign_in_path_for(resource)
    rails_admin.index_path('product')
  end

  def after_sign_out_path_for(resource)
    products_path
  end
end
