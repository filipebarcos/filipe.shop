class ProductsController < ApplicationController
  def index
    @products = Product
      .not_sold
      .with_image
      .order(:room, :price)
      .all
  end
end
