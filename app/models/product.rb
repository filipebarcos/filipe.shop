class Product < ApplicationRecord

  ROOMS = {
    'kitchen'        => 'Cozinha',
    'office'         => 'Escritorio',
    'living-room'    => 'Sala/varanda',
    'master-bedroom' => 'Quarto Casal',
    'baby-room'      => 'Quarto Samuel',
    'service-area'   => 'Area de Servico',
  }

  mount_uploader :image, ImageUploader
  validates :price,
            :title,
            :room,
            presence: true

  validates :room, inclusion: { in: ROOMS.keys }

  scope :not_sold, -> { where(sold_at: nil) }
  scope :sold, -> { where(sold_at: nil) }
  scope :reserved, -> { not_sold.where.not(sold_to: nil) }
  scope :with_image, -> { where.not(image: nil) }
  scope :without_image, -> { where(image: nil) }

  def sold?
    !!sold_at
  end

  def live?
    !image.blank? && sold_at.blank?
  end

  def sell!(to:, sold_price: nil)
    value = sold_price.nil? ? price : sold_price
    update!(
      sold_price: value,
      sold_to: to,
      sold_at: Time.now.utc,
    )
  end
end
