RailsAdmin.config do |config|
  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  config.included_models = ['Product']
  config.model Product do
    list do
      field :title
      field :price do
        formatted_value do
          "R$ #{value},00"
        end
      end
      field :room do
        formatted_value do
          abstract_model.model::ROOMS[value]
        end
      end
      field :live?
      field :sold?
      field :price_sold do
        formatted_value do
          "R$ #{value},00"
        end
      end
      field :sold_to
    end

    create do
      field :title
      field :description
      field :price
      field :room, :enum do
        enum do
          abstract_model.model::ROOMS.invert
        end
      end
      field :image
    end

    edit do
      field :title
      field :description
      field :price
      field :room, :enum do
        enum do
          abstract_model.model::ROOMS.invert
        end
      end
      field :price_sold
      field :sold_to
      field :sold_at
      field :image
    end
  end

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
end
