Rails.application.routes.draw do
  devise_for :users, only: :sessions
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :products, only: [:index]


  if ENV.fetch('RELEASED', 'false') == 'true'
    root to: "products#index"
  else
    root to: "home#index"
  end
end
