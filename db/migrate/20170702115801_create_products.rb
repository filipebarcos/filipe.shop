class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :title, null: false, unique: true, index: true
      t.string :description
      t.string :room, null: false
      t.integer :price, null: false
      t.integer :price_sold
      t.string :sold_to
      t.datetime :sold_at

      t.timestamps
    end
  end
end
