class ChangeImagesFromProducts < ActiveRecord::Migration[5.1]
  def change
    remove_column :products, :images
    add_column :products, :image, :string
  end
end
