class ChangeDescriptionFromProducts < ActiveRecord::Migration[5.1]
  def up
    rename_column :products, :description, :__description
    add_column :products, :description, :text, null: true
    execute "UPDATE products SET description = __description;"
    remove_column :products, :__description
  end

  def down
    add_column :products, :__description, :string, null: true
    execute "UPDATE products SET __description = description;"
    remove_column :products, :description
  end
end
