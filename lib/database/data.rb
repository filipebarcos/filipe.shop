module Database
  class Data
    def initialize(filename)
      @filename = filename
    end

    def import
      CSV.foreach(@filename, headers: false) do |row|
        if row[1].downcase == "vender" && row[3]
          Product.create!(
            title: row[0],
            room: row[4],
            price: row[3],
          )
        end
      end
    end
  end
end
