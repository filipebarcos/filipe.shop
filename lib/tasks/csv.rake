require 'csv'
namespace :csv do
  task import: :environment do
    filename = ENV['FILE']
    if filename
      Database::Data.new(filename).import
      puts "Done importing your file #{filename}"
    else
      puts "Please, provide FILE environment variables"
    end
  end
end

